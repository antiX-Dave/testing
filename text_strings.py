#!/usr/bin/env python3
import gettext

_ = gettext.gettext
gettext.bindtextdomain("desktop-menu","/usr/share/locale")
gettext.textdomain("desktop-menu")

# String-Position: Command line help message, description of --setup option
setup=_("This option creates the conditions for the programme to function correctly. In detail, the needed symbolic links in the /home directory of the user are corrected or created, as well as the necessary rights changes to the base directory are made and user-specific subfolders are created. If no configuration file specific to the operating-system exists, one is created with the default values {0} and {1}").format("--order=n", "--category-filter=n")
