#!/usr/bin/env python3

import pwd

userid = 1000

while True:
    try:
        print(pwd.getpwuid(userid).pw_name)
        print(pwd.getpwuid(userid).pw_dir)
        userid += 1
    except:
        print ("No more users")
        break
